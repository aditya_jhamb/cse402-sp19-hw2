/* Q is a small query language for JavaScript.
 *
 * Q supports simple queries over a JSON-style data structure,
 * kinda like the functional version of LINQ in C#.
 *
 */

// This class represents all AST Nodes.
export class ASTNode {
    public readonly type: string;

    constructor(type: string) {
        this.type = type;
    }

    execute(data: any[]): any {
        throw new Error("Execute not implemented for " + this.type + " node.");
    }

    optimize(): ASTNode {
        return this;
    }

    run(data: any[]): any {
        return this.optimize().execute(data);
    }

    //// 1.5 implement call-chaining

    filter(predicate: (datum: any) => boolean): ASTNode {
        return new ThenNode(this, new FilterNode(predicate));
    }

    apply(callback: (datum: any) => any): ASTNode {
        return new ThenNode(this, new ApplyNode(callback));
    }

    count(): ASTNode {
        return new ThenNode(this, new CountNode());
    }

    product(query: ASTNode): ASTNode {
        return new CartesianProductNode(this, query);
    }

    join(query: ASTNode, relation: string | ((left: any, right: any) => any)): ASTNode {
        return new JoinNode(this, query, relation);
    }
}

// The Id node just outputs all records from input.
export class IdNode extends ASTNode {

    constructor() {
        super("Id");
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        return data;
    }
}

// We can use an Id node as a convenient starting point for
// the call-chaining interface.
export const Q = new IdNode();

// The Filter node uses a callback to throw out some records.
export class FilterNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (datum: any) => boolean) {
        super("Filter");
        this.predicate = predicate;
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        return data.filter((datum) => this.predicate(datum));
    }
}

// The Then node chains multiple actions on one data structure.
export class ThenNode extends ASTNode {
    first: ASTNode;
    second: ASTNode;

    constructor(first: ASTNode, second: ASTNode) {
        super("Then");
        this.first = first;
        this.second = second;
    }

    //// 1.1 implement execute
    execute(data: any[]): any {
        return this.second.execute(this.first.execute(data));
    }

    optimize(): ASTNode {
        return new ThenNode(this.first.optimize(), this.second.optimize());
    }
}

//// 1.3 implement Apply and Count Nodes

export class ApplyNode extends ASTNode {
    callback: (datum: any) => any;

    constructor(callback: (datum: any) => any) {
        super("Apply");
        this.callback = callback;
    }

    execute(data: any[]): any {
        return data.map((x) => (this.callback(x)));
    }
}

export class CountNode extends ASTNode {
    constructor() {
        super("Count");
    }

    execute(data: any[]): any {
        return [data.length];
    }
}

//// 2.1 optimize queries

// This function permanently adds a new optimization function to a node type.
// An optimization function takes in a node and either returns a new,
// optimized node or null if no optimizations can be performed. AddOptimization
// will register this function to a particular node type, so that it will be called
// as part of that node's optimize() method, along with all other registered
// optimizations.
function AddOptimization(nodeType, opt: (this: ASTNode) => ASTNode | null) {
    const oldOptimize = nodeType.prototype.optimize;
    nodeType.prototype.optimize = function(this: ASTNode): ASTNode {
        const newThis = oldOptimize.call(this);
        return opt.call(newThis) || newThis;
    };
}

// For example, the following small optimization removes unnecessary Id nodes.
AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
    if (this.first instanceof IdNode) {
        return this.second;
    } else if (this.second instanceof IdNode) {
        return this.first;
    } else {
        return null;
    }
});

// The above optimization has a few notable side effects. First, it removes the
// root IdNode from your call chain, so if you were depending on that to exist
// your other optimizations may be confused. Also, it returns the interesting
// subtree directly, without trying to optimize it more. It is up to you to
// determine where additional optimization should occur.

// We won't specifically test for this optimization, so if it's giving you
// a headache feel free to comment it out.

// You can add optimizations for part 2.1 here (or anywhere below).
// ...
AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
   if (this instanceof ThenNode) {
       const firstOpt: ASTNode = this.first;
       const secondOpt: ASTNode = this.second;
       if (firstOpt instanceof FilterNode && secondOpt instanceof FilterNode) {
           return new FilterNode((x) => (firstOpt.predicate(x) && secondOpt.predicate(x)));
       }
   }
   return null;
});

AddOptimization(ThenNode, function(this: ThenNode): ASTNode {
   if (this instanceof ThenNode) {
       const firstOpt: ASTNode = this.first;
       const secondOpt: ASTNode = this.second;
       if (firstOpt instanceof CountNode && secondOpt instanceof FilterNode)  {
           return new CountIfNode(secondOpt.predicate);
       } else if (firstOpt instanceof FilterNode && secondOpt instanceof CountNode) {
           return new CountIfNode(firstOpt.predicate);
       }
       return null;
   }
});

//// 2.2 internal node types and CountIf

export class CountIfNode extends ASTNode {
    predicate: (datum: any) => boolean;

    constructor(predicate: (datum: any) => boolean) {
        super("CountIf");
        this.predicate = predicate;
    }

    execute(data: any[]): any {
        let count: number = 0;
        data.forEach((x) => {
            if (this.predicate(x)) {
                count++;
            }
        });
        return {count};
    }
}

//// 3.1 cartesian products

export class CartesianProductNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;

    constructor(left: ASTNode, right: ASTNode) {
        super("CartesianProduct");
        this.left = left;
        this.right = right;
    }

    optimize(): ASTNode {
        return new CartesianProductNode(this.left.optimize(), this.right.optimize());
    }

    execute(data: any[]): any {
        const result = [];
        const left = this.left.execute(data);
        const right = this.right.execute(data);
        left.forEach((x) => {
            right.forEach((y) => {
                result.push({left: x, right: y});
            });
        });
        return result;
    }
}

//// 3.2-3.6 joins and hash joins

/* Hint: Recall from the spec that a join of two arrays P and Q by a
   function f(l, r) is the array with one entry for each pair of a record
   in P and Q for which f returns true and that each entry should have all
   fields and values that either record in the pair had.

   Most notably, if both records had the same field, use the value from the
   record from Q. In JavaScript, this can be achieved with Object.assign:
   https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
*/

export class JoinNode extends ASTNode {
    relation;
    left: ASTNode;
    right: ASTNode;
    field;
    
    constructor(left: ASTNode, right:ASTNode, relation: string | ((left: any, right: any) => any)) {
        super("Join");
        this.left = left;
        this.right = right;
        if (typeof(this.relation) === "string") {
            this.field = relation;
        } else {
            this.relation = relation;
            this.field == "";
        }
    }

    optimize(): ASTNode {
        return new JoinNode(this.left.optimize(), this.right.optimize(), this.relation);
    }

    execute(data: any[]): any {
        const left = this.left.execute(data);
        const right = this.right.execute(data);

        var result = [];
        if(this.field !== "") {
            left.forEach((x) => {
                right.forEach((y) => {
                    if(x[this.field] === y[this.field]) {
                        result.push(Object.assign(x,y));
                    }
                    
                });
            });
            return result;
        }
        left.forEach((x) => {
            right.forEach((y) => {
                if(this.relation(x,y)) {
                    result.push(Object.assign(x,y));
                }
                
            });
        });
        
        return result;
    }
}

/* Hint: While optimizing the fluent join to use your internal JoinNode,
   if you find yourself needing to compare two functions for equality,
   you can do so with the Javascript operator `===` (pointer equality).
 */

export class HashJoinNode extends ASTNode {
    left: ASTNode;
    right: ASTNode;
    field: string;
    constructor(field: string, left: ASTNode, right:ASTNode) { // you may want to add some proper arguments to this constructor
        super("HashJoin");
        this.left = left;
        this.right = right;
        this.field = field;
    }

    optimize(): ASTNode {
        return new HashJoinNode(this.field, this.left.optimize(), this.right.optimize());
    }

    execute(data: any[]): any {
        const left = this.left.execute(data);
        const right = this.right.execute(data);

        var leftMap = new Map();
        var rightMap = new Map();
        console.log(left);

        left.forEach((x) => {
            if(leftMap.has(x[this.field])) {
                var newVal = Object.assign(leftMap.get(x[this.field]), x);
                leftMap.set(x[this.field], newVal);
            } else {
                leftMap.set(x[this.field], x);
            }
        });
        right.forEach((x) => {
            if(rightMap.has(x[this.field])) {
                var newVal = Object.assign(rightMap.get(x[this.field]), x);
                rightMap.set(x[this.field], newVal);
            } else {
                rightMap.set(x[this.field], x);
            }
        });

        var data = [];
        leftMap.forEach((value, key, map) => {
            if(rightMap.has(key)) {
                data.push(Object.assign(value, rightMap.get(key)));
            }
        });
        return data;
    }
}
