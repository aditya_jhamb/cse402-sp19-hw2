import * as q from "./q";
const Q = q.Q;

//// 1.2 write a query

export const theftsQuery = new q.FilterNode((x) => (x[2].includes("THEFT")));
export const autoTheftsQuery = new q.FilterNode((x) => x[2].includes("VEH-THEFT"));

//// 1.4 clean the data

export const cleanupQuery = new q.ApplyNode((x) => ({description : x[2], category: x[3], area: x[4], date: x[5]}));

//// 1.6 reimplement queries with call-chaining

export const cleanupQuery2 = Q.apply((x) => ({description : x[2], category: x[3], area: x[4], date: x[5]}));
export const theftsQuery2 = Q.filter((x) => (x.description.includes("THEFT")));
export const autoTheftsQuery2 = Q.filter((x) => (x.description.includes("VEH-THEFT")));

//// 4 put your queries here (remember to export them for use in tests)

export const bigPrimes = Q.filter((x) => (x > 1000)).join(Q, (x,y) => (x > y) && (x < 1500));
export const smallPrimesProducts = Q.filter((x) => (x < 100)).product(Q.filter((x) => x < 50)).count();
export const callme = Q.filter((x) => x < 100).apply((x) => 2 * x);
export const hereonesecond = Q.filter((x) => x > 100).apply((x) => "whoops").apply((x) => x + " Im gone");
