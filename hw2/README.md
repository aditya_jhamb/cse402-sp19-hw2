This directory contains a starter kit for Homework 2, "a Query Language".

The query engine skeleton is in the src/ directory. src/q.ts contains the
engine itself, and you will use it to implement some queries in src/queries.ts.

The provided tests are in test/q.ts. They are based on the data in
tests/data.ts. You should provide additional tests in test/custom.ts; the
framework of the first test has been included as an example. As with
HW1, you can run the TypeScript compiler and all the tests with `npm test`.

If you want to use your own data, you can provide it as a separate file
or add it to tests/data.ts. If you want to load real json rather than
copying it into a source file and pretending it's a JavaScript object,
so much the better.
