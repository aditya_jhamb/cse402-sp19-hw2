import {expect} from "chai";

import * as q from "../src/q";
import * as queries from "../src/queries";
const Q = q.Q;
import {primes} from "./data";

// You should provide at least 5 tests, using your own queries from Part 4
// and either the crime data from test/data.ts or your own data. You can look
// at test/q.ts to see how to import and use the queries and data.
// The outline of the first test has been provided as an example - you should
// modify it so it actually does something interesting.

describe("custom", () => {

    it("gets big primes", () => {
        const query = queries.bigPrimes;
        var result = query.execute(primes);
        expect(result.length).to.be.greaterThan(5000);
    });

    it("tests small primes nothing passes", () => {
        const query = queries.smallPrimesProducts;
        var result = query.execute(primes);
        expect(result.length).to.be.greaterThan(-1);
    });

    it("test calling from apply", () => {
        const query = queries.callme;
        var result = query.execute(primes);
        expect(result[0]).to.be.equal(4);
    });

    it("test multitype applies", () => {
        const query = queries.hereonesecond;
        var result = query.execute(primes);
        expect(result[0]).to.be.equal("whoops Im gone");
        expect(result[1]).to.be.equal("whoops Im gone");
    });

    it("test changing query", () => {
        const query = queries.hereonesecond.count();
        var result = query.execute(primes);
        expect(result[0]).to.be.equal(1204);
    });
});
